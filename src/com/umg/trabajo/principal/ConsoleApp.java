package com.umg.trabajo.principal;

import com.umg.trabajo.clases.Empresa;
import com.umg.trabajo.clases.Lugar;
import com.umg.trabajo.clases.Mantenimiento;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Maco on 7/07/2017.
 */
public class ConsoleApp {

    public static void main(String[] args) throws Exception {
        Empresa empresa = null;
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {

            System.out.println("1. Ingrese cantidad de lugares para la fundación");
            System.out.println("2. Los datos del Mantenimiento");
            System.out.println("3. Adicionar Lugar");
            System.out.println("4. Listar si van fuera o dentro de la fundacion");
            System.out.println("5. Adicionar personal");
            System.out.println("6. Listar Personal");
            System.out.println("7. Salir");

            System.out.println("Sistema para Fundación Familias Mayas");

            try {
                System.out.println("Seleccione una de las opciones dadas");
                opcion = sn.nextInt();

                switch (opcion) {

                    case 1:
                        System.out.println("Cuantos lugares posee la Fundacion");
                        empresa = new Empresa(sn.nextInt());
                        break;

                    case 2:
                        System.out.println("Nombre:");
                        Mantenimiento mantenimiento = new Mantenimiento();
                        mantenimiento.setNombre(sn.next());
                        System.out.println("Años de experiencia:");
                        mantenimiento.setExper(sn.nextInt());
                        break;

                    case 3:
                        System.out.println("Nombre del lugar:");
                        Lugar lugar = new Lugar(sn.next());
                        System.out.println("es dentro? (S/N):");
                        if (sn.next().equalsIgnoreCase("S"))
                            lugar.setFueradentro(true);
                        empresa.adicionarLugar(lugar);

                    case 4:
                        Lugar[] listado = empresa.getListado();
                        for (int i = 0; i <empresa.getCantidad() ; i++) {
                            if(listado[i].isFueradentro())
                                System.out.printf("Lugar #"+(i+1)+ ": "+listado[i].getNombre()+"\n");
                        }
                        System.out.println("--------------");
                        break;

                    case 5:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 5");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }
    }
}