package com.umg.trabajo.clases;

/**
 * Created by Maco on 7/07/2017.
 */
public class Personal {
    private String nombre;
    private int edad;

    public Personal(String nombre) {this.nombre = nombre;}

    public Personal(int edad) {this.edad = edad;}

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {this.nombre = nombre;}

    public int getEdad() {return edad;}

    public void setEdad(int edad) {this.edad = edad;}
}
