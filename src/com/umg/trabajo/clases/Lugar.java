package com.umg.trabajo.clases;

/**
 * Created by Maco on 7/07/2017.
 */
public class Lugar {
    private String nombre;
    private boolean fueradentro;

    public Lugar(String nombre) {this.nombre = nombre;}

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {this.nombre = nombre;}

    public boolean isFueradentro() {return fueradentro;}

    public void setFueradentro(boolean fueradentro) {this.fueradentro = fueradentro;}

}
