package com.umg.trabajo.clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maco on 7/07/2017.
 */
public class Empresa {
    private Lugar[] listado;
    private List<Personal> listadoPersonal;
    private Mantenimiento mantenimiento;
    private int cantidad;

    public Empresa(int cantLugar) {
        listado = new Lugar[cantLugar];
        cantidad = 0;
        listadoPersonal = new ArrayList<>();

    }

    public Lugar[] getListado() {return listado;}

    public void setListado(Lugar[] listado) {this.listado = listado;}

    public Mantenimiento getMantenimiento() {return mantenimiento;}

    public void setMantenimiento(Mantenimiento mantenimiento) {this.mantenimiento = mantenimiento;}

    public int getCantidad() {return cantidad;}

    public void setCantidad(int cantidad) {this.cantidad = cantidad;}

    public List<Personal> getListadoPersonal() {return listadoPersonal;}

    public void adicionarLugar(Lugar d) throws Exception {

        if (cantidad < listado.length) {

            if (d.isFueradentro()) {
                for (int i = cantidad; i > 0; i--) {
                    listado[i] = listado[i - 1];
                }
                listado[0] = d;
            } else
                listado[cantidad] = d;
            cantidad++;

        } else
            throw new Exception("Imposible adicionar más Lugares");


    }

}


