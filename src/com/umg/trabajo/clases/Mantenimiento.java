package com.umg.trabajo.clases;

/**
 * Created by Maco on 7/07/2017.
 */

    public class Mantenimiento {
        private String nombre;
        private int exper;

        public Mantenimiento() {
        }

        public String getNombre() {return nombre;}

        public void setNombre(String nombre) {this.nombre = nombre;}

        public int getExper() {return exper;}

        public void setExper(int exper) {this.exper = exper;}
    }

